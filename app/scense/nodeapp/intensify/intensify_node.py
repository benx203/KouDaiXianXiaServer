#coding:utf8
'''
Created on 2013-10-29

@author: ywq
'''

from firefly.server.globalobject import remoteserviceHandle
#from app.scense.protofile.intensify import GetIntensifyInfo1401_pb2
from app.scense.applyInterface.intensify_app import getIntensifyInfo1401,intensifyPet1402
from app import js

@remoteserviceHandle('gate')
def getIntensifyInfo_1401(did,data):
    '''获取强化界面信息
    '''
    info = js.load(data)
    pid = info.get("pid")
    response = js.Obj()
    getIntensifyInfo1401(pid,response)
    return js.objstr(response)
#     response.result = data.get('result',False)
#     response.message = data.get('message','')
#     info = data.get('data',None)
#     if info:
#         response.data = []
#         dainfo = js.Obj()
#         dainfo.fighting = info.get('fighting')
#         dainfo.silvermoney = info.get('silvermoney')
#         dainfo.luck = info.get('luck')
#         tinfo = info.get('teaminfo')
#         if tinfo:
#             dainfo.teaminfo = []
#             for i in tinfo.values():
#                 teinfo = js.Obj()
#                 teinfo.name = i['name']
#                 teinfo.level= i['level']
#                 teinfo.levellimit= i['levellimit']
#                 teinfo.resource = i['resource']
#                 teinfo.headid = i['headid']
#                 teinfo.color = i['color']
#                 teinfo.hp = i['hp']
#                 teinfo.hpmoney = i['hpmoney']
#                 teinfo.att = i['att']
#                 teinfo.attmoney = i['attmoney']
#                 teinfo.status = i['status']
#                 dainfo.teaminfo.append(teinfo)
#         response.data.append(dainfo)
#    return js.objstr(response)

@remoteserviceHandle('gate')
def intensifyPet_1402(did,data):
    '''强化宠物
    '''
    info = js.load(data)
    pid = info.get("pid")
    petid = info.get("petid")
    typeid = info.get("typeid")
    response = js.Obj()
    data = intensifyPet1402(pid,petid,typeid,response)
    return js.objstr(response)
#     response.result = data.get('result',False)
#     response.message = data.get('message','')
#     info = data.get('data',None)
#     if info:
#         response.data = []
#         dainfo = js.Obj()
#         dainfo.intensifyResult = info.get('intensifyResult')
#         dainfo.money = info.get('money')
#         dainfo.addhp = info.get('addhp')
#         dainfo.hpmoney = info.get('hpmoney')
#         dainfo.addatt = info.get('addatt')
#         dainfo.attmoney = info.get('attmoney')
#         dainfo.djmoney = info.get('djmoney')
#         dainfo.newfighting = info.get('newfighting')
#         dainfo.newcolor = info.get('newcolor')
#         dainfo.newlevellimit = info.get('newlevellimit')
#         dainfo.luck = info.get('luck')
#         response.data.append(dainfo)
#    return js.objstr(response)


